package com.nespresso.sofa.interview.parking;

import java.util.ArrayList;
import java.util.List;

import com.nespresso.sofa.interview.parking.util.Util;

/**
 * Handles the parking mechanisms: park/unpark a car (also for disabled-only bays) and provides a string representation of its state.
 */
public class Parking extends ParkingBuilder {

	private List<Integer> totalPlacesOccupied = new ArrayList<Integer>();
	private List<Integer> totalPlacesDisabledOccupied = new ArrayList<Integer>();
	private int numberPlacesLeft = 0;
	private char[][] squareBuild;
	private int[][] square;
	private List<Integer> pedestrianExitIndex;
	private List<Integer> disabledBayIndex;
	
	Parking(){
		this.totalPlacesDisabledOccupied =new ArrayList<Integer>();
		this.totalPlacesOccupied =new ArrayList<Integer>();
		this.numberPlacesLeft = 0;
		this.squareBuild = new char[1][1];
		this.square = new int[1][1];
		this.pedestrianExitIndex =new ArrayList<Integer>();
		this.disabledBayIndex=new ArrayList<Integer>();
	}
    /**
     * @return the number of available parking bays left
     */
    public int getAvailableBays() {
    	int numberOfPlaces = this.squareBuild.length * this.squareBuild[0].length;
    	
    	//Discount the disabled bays
    	if(this.disabledBayIndex !=null && !this.disabledBayIndex.isEmpty()){
    		numberOfPlaces -= this.disabledBayIndex.size();
    	}
    	
    	//Discount the pedestrian exits
    	if(this.pedestrianExitIndex != null && !this.pedestrianExitIndex.isEmpty()){
    		numberOfPlaces -= this.pedestrianExitIndex.size();
    	}
    	
    	//Discount the places occupied
    	if(!totalPlacesOccupied.isEmpty()){
    		numberOfPlaces -= totalPlacesOccupied.size();
    	}
    	
    	//Places left
    	this.numberPlacesLeft = (this.squareBuild.length * this.squareBuild[0].length) - numberOfPlaces;
    	
    	return numberOfPlaces;
    }

    /**
     * Park the car of the given type ('D' being dedicated to disabled people) in closest -to pedestrian exit- and first (starting from the parking's entrance)
     * available bay. Disabled people can only park on dedicated bays.
     *
     *
     * @param carType
     *            the car char representation that has to be parked
     * @return bay index of the parked car, -1 if no applicable bay found
     */
    public int parkCar(final char carType) {
    	int parkingCarIndex = -1;
    	//If we had available bays
    	if(numberPlacesLeft>0){
    		int index = -1;
    		//Disable && you had free disabled places
    		if(carType == 'D' && this.totalPlacesDisabledOccupied.size() < this.disabledBayIndex.size()){
    			//Calculate the index free
    			index = Util.calculateParkIndex(carType, this.squareBuild);
				//Add the index you park the car to the disabled and occupied places
    			this.totalPlacesDisabledOccupied.add(index);
    			//parkingPlaces
    		}else{
    			//Other types Calculate the index free
    			index = Util.calculateParkIndex(carType, this.squareBuild);
    		}
    		if(index != -1){
	    		this.totalPlacesOccupied.add(index);
	    		parkingCarIndex = index;
	    		this.numberPlacesLeft--;
    		}
    	}else{
    		//No available bay found
    		return parkingCarIndex;
    	}
    	return parkingCarIndex;
    }

    /**
     * Unpark the car from the given index
     *
     * @param index
     * @return true if a car was parked in the bay, false otherwise
     */
    public boolean unparkCar(final int index) {
    	boolean unpark = false;
    	
    	//If the car are parked in the bay
    	int indexTotal = this.totalPlacesOccupied.indexOf(index);
    	if(indexTotal>0){
    		this.totalPlacesOccupied.remove(indexTotal);
    		//One more place free
    		this.numberPlacesLeft ++;
    		unpark = true;
    	}
    	
    	//If the car park in a disabled bay the one more disabled place free
    	if(this.disabledBayIndex.contains(index)){
    		int indexDisable = this.totalPlacesDisabledOccupied.indexOf(index);
    		if(indexDisable>0){
    			this.totalPlacesDisabledOccupied.remove(indexDisable);
    		}
    	}
    	return unpark;
    }

    /**
     * Print a 2-dimensional representation of the parking with the following rules:
     * <ul>
     * <li>'=' is a pedestrian exit
     * <li>'@' is a disabled-only empty bay
     * <li>'U' is a non-disabled empty bay
     * <li>'D' is a disabled-only occupied bay
     * <li>the char representation of a parked vehicle for non-empty bays.
     * </ul>
     * U, D, @ and = can be considered as reserved chars.
     *
     * Once an end of lane is reached, then the next lane is reversed (to represent the fact that cars need to turn around)
     *
     * @return the string representation of the parking as a 2-dimensional square. Note that cars do a U turn to continue to the next lane.
     */
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	for(int i=0;i<this.square.length;i++){
    		for(int j=0;j<this.square[i].length;j++){
    			sb.append(square[i][j]);
    		}
    		sb.append("\\n");
    	}
    	return sb.toString();
    }

	public List<Integer> getTotalPlacesOccupied() {
		return totalPlacesOccupied;
	}

	public void setTotalPlacesOccupied(List<Integer> totalPlacesOccupied) {
		this.totalPlacesOccupied = totalPlacesOccupied;
	}

	public List<Integer> getTotalPlacesDisabledOccupied() {
		return totalPlacesDisabledOccupied;
	}

	public void setTotalPlacesDisabledOccupied(List<Integer> totalPlacesDisabledOccupied) {
		this.totalPlacesDisabledOccupied = totalPlacesDisabledOccupied;
	}

	public int getNumberPlacesLeft() {
		return numberPlacesLeft;
	}

	public void setNumberPlacesLeft(int numberPlacesLeft) {
		this.numberPlacesLeft = numberPlacesLeft;
	}

	public char[][] getSquareBuild() {
		return squareBuild;
	}

	public void setSquareBuild(char[][] squareBuild) {
		this.squareBuild = squareBuild;
	}

	public int[][] getSquare() {
		return square;
	}
	public void setSquare(int[][] square) {
		this.square = square;
	}
	public List<Integer> getPedestrianExitIndex() {
		if(pedestrianExitIndex == null || pedestrianExitIndex.isEmpty()){
			pedestrianExitIndex = new ArrayList<Integer>();
		}
		return pedestrianExitIndex;
	}

	public void setPedestrianExitIndex(List<Integer> pedestrianExitIndex) {
		this.pedestrianExitIndex = pedestrianExitIndex;
	}

	public List<Integer> getDisabledBayIndex() {
		if(disabledBayIndex == null || disabledBayIndex.isEmpty()){
			disabledBayIndex = new ArrayList<Integer>();
		}
		return disabledBayIndex;
	}

	public void setDisabledBayIndex(List<Integer> disabledBayIndex) {
		this.disabledBayIndex = disabledBayIndex;
	}

}
