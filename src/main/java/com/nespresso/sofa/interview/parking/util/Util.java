package com.nespresso.sofa.interview.parking.util;

import java.util.HashMap;

public class Util {

	public static int calculateParkIndex(char carType, char[][] square){
		int index=0;
		HashMap <Integer,Integer> lstPlacesFree = new HashMap<Integer, Integer>();
		for(int i=0;i<square.length;i++){
			for(int j=0;j<square[i].length;j++){
				if(square[i][j]==' '){
					lstPlacesFree.put(i, j);
				}
				if(square[i][j]=='=' && carType != 'D'){
					if(i>0 && lstPlacesFree.containsKey(i-1) && lstPlacesFree.get(i-1).intValue()==j && square[i-1][j]==' '){
						index = (i-1) + j;
					}else if(j>0 && lstPlacesFree.containsKey(i) && lstPlacesFree.get(i).intValue()==(j-1) && square[i-1][j]==' '){
							index = (j-1) + i;
					}
				}else{
					if(square[i][j]=='@' && carType == 'D'){
						if(i>0 && lstPlacesFree.containsKey(i-1) && lstPlacesFree.get(i-1).intValue()==j && square[i-1][j]==' '){
							index = (i-1) + j;
						}else if(j>0 && lstPlacesFree.containsKey(i) && lstPlacesFree.get(i).intValue()==(j-1) && square[i-1][j]==' '){
								index = (j-1) + i;
						}
					}
				}
			}
		}
	
		return index;
	}
	
}
