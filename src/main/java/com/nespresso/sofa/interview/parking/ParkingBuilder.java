package com.nespresso.sofa.interview.parking;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder class to get a parking instance
 */
public class ParkingBuilder {

	private List<Integer> lstPedestrianExits;
	private int[][] squareSize;
	private List<Integer> lstDisabledBay;
	
	ParkingBuilder(){
		this.lstDisabledBay = new ArrayList<Integer>();
		this.lstPedestrianExits = new ArrayList<Integer>();
		this.squareSize = new int[1][1];
	}
	
    public ParkingBuilder withSquareSize(final int size) {
    	this.squareSize = new int[size][size];
    	return this;
    }

    public ParkingBuilder withPedestrianExit(final int pedestrianExitIndex) {
    	//If is the firstTime we create the list.
    	if(this.lstPedestrianExits == null || this.lstPedestrianExits.isEmpty()){
    		this.lstPedestrianExits = new ArrayList<Integer>();
    	}
    	this.lstPedestrianExits.add(pedestrianExitIndex);
    	
        return this;
    }

    public ParkingBuilder withDisabledBay(final int disabledBayIndex) {
    	//If is the firstTime we create the list.
    	if(this.lstDisabledBay == null || this.lstDisabledBay.isEmpty()){
    		this.lstDisabledBay = new ArrayList<Integer>();
    	}
    	this.lstDisabledBay.add(disabledBayIndex);

        return this;
    }

    public Parking build() {
    	int[][] squareBuild = this.squareSize.clone();
    	char[][] square = new char[squareSize.length][squareSize[0].length];
    	for(int i=0;i<this.squareSize.length;i++){
    		for(int j=0;j<this.squareSize[i].length;j++){
		    	if(this.lstPedestrianExits != null && !this.lstPedestrianExits.isEmpty()){
		    		square[i][j] = '=';
		    	}else if(this.lstDisabledBay != null && !this.lstDisabledBay.isEmpty()){
			    		square[i][j] = '@';
			    	}else{
			    		square[i][j] = ' ';
			    	}
    		}
		}
    	//Creating the Parking object
    	Parking p = new Parking();
    	if(this.lstPedestrianExits != null && !this.lstPedestrianExits.isEmpty()){
    		p.getPedestrianExitIndex().addAll(lstPedestrianExits);
    	}
    	if(this.lstDisabledBay != null && !this.lstDisabledBay.isEmpty()){
    		p.getDisabledBayIndex().addAll(lstDisabledBay);
    	}
    	p.setSquare(squareBuild);
    	p.setSquareBuild(square);
    	return p;
    }

	public List<Integer> getLstPedestrianExits() {
		return lstPedestrianExits;
	}

	public void setLstPedestrianExits(List<Integer> lstPedestrianExits) {
		this.lstPedestrianExits = lstPedestrianExits;
	}

	public int[][] getSquareSize() {
		return squareSize;
	}

	public void setSquareSize(int[][] squareSize) {
		this.squareSize = squareSize;
	}

	public List<Integer> getLstDisabledBay() {
		return lstDisabledBay;
	}

	public void setLstDisabledBay(List<Integer> lstDisabledBay) {
		this.lstDisabledBay = lstDisabledBay;
	}
}
